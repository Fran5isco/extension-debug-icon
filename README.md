# Extension-Debug-Icon

## Installation  
- download the project from https://bitbucket.org/Fran5isco/extension-debug-icon/get/955a1de747d9.zip
- go to web: `chrome://extensions/`   
- switch on: `Developer Mode`  
- click `LOAD UNPACKED` button   
- search the folder and click `SELECT` button  
- open your app

## How it works
Open your app in the browser, if debug icon is in red, you´re already in debug mode, if the icon is in gray you are'nt.  
Click the icon if it's in gray to switch to debug mode, you'll the icon turns red.  