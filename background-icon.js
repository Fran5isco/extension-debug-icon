'use strict';
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {

        if(request.changeIcon) {
            chrome.browserAction.setIcon({
                path:"icon1.png",
                tabId: sender.tab.id
            });
            setTimeout(function(){
                chrome.browserAction.setIcon({
                    path:"icon2.png",
                    tabId: sender.tab.id
                });
            }, 5000);
        }

    }
);
